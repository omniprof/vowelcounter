package com.cejv569.vowelcounter.exception;

/**
 * Custom named checked exception that must be caught
 * 
 * @author Ken Fogel
 */
public class InvalidCharactersInStringException extends Exception{
    
    public InvalidCharactersInStringException(String errorMessage) {
        super(errorMessage);
    }
    
}
