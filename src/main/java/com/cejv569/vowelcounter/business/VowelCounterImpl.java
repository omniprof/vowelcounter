package com.cejv569.vowelcounter.business;

import com.cejv569.vowelcounter.exception.InvalidCharactersInStringException;
import java.util.HashSet;
import java.util.regex.Pattern;

/**
 * Vowel Counter
 *
 * A, E, I, O, U and:
 *
 * Y is considered to be a vowel if…
 *
 * The word has no other vowel: gym, my.
 *
 * The letter is at the end of a word or syllable: candy, deny, bicycle,
 * acrylic.
 *
 * The letter is in the middle of a syllable: system, borborygmus.
 *
 * From:
 * https://www.merriam-webster.com/words-at-play/why-y-is-sometimes-a-vowel-usage
 *
 * @author Ken Fogel
 */
public class VowelCounterImpl implements VowelCounter {

    private final HashSet<Character> valid;

    public VowelCounterImpl() {
        valid = new HashSet<>();
        valid.add('a');
        valid.add('e');
        valid.add('i');
        valid.add('o');
        valid.add('u');
        valid.add('y');
    }

    /**
     * Method to count the number of vowels in a word. It is missing the proper
     * handling of 'y'. If the string it receives contains anything other than
     * letters, upper ot lower case, then it throws a custom checked exception.
     *
     * @param vowelWord The string to test for vowels
     * @return number of vowels in the word
     * @throws InvalidCharactersInStringException
     */
    @Override
    public int doCount(final String vowelWord) throws InvalidCharactersInStringException {
        // Convert to uppercase
        String testWord = vowelWord.toUpperCase();
        int vowelCounter = 0;
        if (Pattern.matches("^[A-Z]+$", testWord)) {
            char[] word = vowelWord.toCharArray();
            for (char ch : word) {
                if (valid.contains(ch)) {
                    ++vowelCounter;
                }
            }
        } else {
            throw new InvalidCharactersInStringException(vowelWord);
        }
        return vowelCounter;
    }
}
