package com.cejv569.vowelcounter;

import com.cejv569.vowelcounter.business.VowelCounter;
import com.cejv569.vowelcounter.business.VowelCounterImpl;
import com.cejv569.vowelcounter.exception.InvalidCharactersInStringException;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * This class tests if the method doCount in the VowelCounter class works
 *
 * @author Ken Fogel
 */
public class MainApp {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    /**
     * Testing code without unit testing
     */
    public void perform() {
        VowelCounter counter = new VowelCounterImpl();

        String testWord = "moose";
        try {
            System.out.println("Number of vowels in '" + testWord + "': " + counter.doCount(testWord));
        } catch (InvalidCharactersInStringException ex) {
            LOG.error("Invalid character in the string '" + ex.getMessage() + "'");
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {

        MainApp mainApp = new MainApp();
        mainApp.perform();
    }
}
